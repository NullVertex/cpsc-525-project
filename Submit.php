<?php
    $survey_code = bin2hex(random_bytes(16));
    setcookie("surveycode", $survey_code, time() + 86400); // Set cookie for 1 day

    $data = file_get_contents('php://input');
    $json = json_decode($data, true);
    $json['survey_code'] = (string)$survey_code;

    if(!empty($data)) {
        $fname = getcwd() . "/results/results.json";
        $file = fopen($fname, 'a+') or die("Couldn't open the file"); //creates or opens file
        
        $contents = ",\n" . json_encode($json);
        fwrite($file, $contents);
        fclose($file);
    }
    exit();
?>