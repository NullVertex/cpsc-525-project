﻿//Requires zxcvbn.js and Bootstrap
(function ($) {

	$.fn.zxcvbnProgressBar = function (options) {

		//init settings
		var settings = $.extend({
			passwordInput: '#Password',
			passwordConfirm: '#Confpass',
			userInputs: [],
			ratings: ["Very weak", "Weak", "OK", "Strong", "Very strong"],
			//all progress bar classes removed before adding score specific css class
			allProgressBarClasses: "progress-bar-danger progress-bar-warning progress-bar-success progress-bar-striped active",
			//bootstrap css classes (0-4 corresponds with zxcvbn score)
			progressBarClass0: "progress-bar-danger progress-bar-striped active",
			progressBarClass1: "progress-bar-danger progress-bar-striped active",
			progressBarClass2: "progress-bar-warning progress-bar-striped active",
			progressBarClass3: "progress-bar-success progress-bar-striped active",
			progressBarClass4: "progress-bar-success progress-bar-striped active"
		}, options);

		return this.each(function () {
			settings.progressBar = this;
			//init progress bar display
			UpdateProgressBar();
			//Update progress bar on each keypress of password input
			$(settings.passwordInput).keyup(function (event) {
				UpdateProgressBar();
			});
			$(settings.passwordConfirm).keyup(function (event) {
				UpdateProgressBar();
			});
		});

		function UpdateProgressBar() {
			var submitButton = document.getElementById("SubmitButton");
			var nextButton = document.getElementById("NextButton");
			var progressBar = settings.progressBar;
			var password = $(settings.passwordInput).val();
			var confpass = $(settings.passwordConfirm).val();
			if (password) {
				var result = zxcvbn(password, settings.userInputs);
				//result.score: 0, 1, 2, 3 or 4 - if crack time is less than 10**2, 10**4, 10**6, 10**8, Infinity.
				var scorePercentage = (result.score + 1) * 20;
				$(progressBar).css('width', scorePercentage + '%');

				// Enforce requirements
				var req1 = (result.score >= 1);
				var req2 = (password.length >= 8);
				var req3 = (password === confpass);

				if (req1) {
					document.getElementById("Req_1_check").style.visibility = "visible";
				}
				else {
					document.getElementById("Req_1_check").style.visibility = "hidden";
				}
				if (req2) {
					document.getElementById("Req_2_check").style.visibility = "visible";
				}
				else {
					document.getElementById("Req_2_check").style.visibility = "hidden";
				}
				if (req3) {
					document.getElementById("Req_3_check").style.visibility = "visible";
				}
				else {
					document.getElementById("Req_3_check").style.visibility = "hidden";
				}

				if (req1 && req2 && req3) {
					nextButton.disabled = false;
				}
				else {
					nextButton.disabled = true;
				}

				// Update the progress bar
				if (result.score == 0) {
					//weak
					$(progressBar).removeClass(settings.allProgressBarClasses).addClass(settings.progressBarClass0);
					$(progressBar).html(settings.ratings[0]);
				}
				else if (result.score == 1) {
					//normal
					$(progressBar).removeClass(settings.allProgressBarClasses).addClass(settings.progressBarClass1);
					$(progressBar).html(settings.ratings[1]);
				}
				else if (result.score == 2) {
					//medium
					$(progressBar).removeClass(settings.allProgressBarClasses).addClass(settings.progressBarClass2);
					$(progressBar).html(settings.ratings[2]);
				}
				else if (result.score == 3) {
					//strong
					$(progressBar).removeClass(settings.allProgressBarClasses).addClass(settings.progressBarClass3);
					$(progressBar).html(settings.ratings[3]);
				}
				else if (result.score == 4) {
					//very strong
					$(progressBar).removeClass(settings.allProgressBarClasses).addClass(settings.progressBarClass4);
					$(progressBar).html(settings.ratings[4]);
				}
			}
			else {
				$(progressBar).css('width', '0%');
				$(progressBar).removeClass(settings.allProgressBarClasses).addClass(settings.progressBarClass0);
				$(progressBar).html('');

				// Defaults
				submitButton.disabled = true;
				document.getElementById("Req_1_check").style.visibility = "hidden";
				document.getElementById("Req_2_check").style.visibility = "hidden";
				document.getElementById("Req_3_check").style.visibility = "hidden";
			}
		}
	};
})(jQuery);

function displaySurveyCode() {
	// Get Posted survey code from server
	var surveycode = getCookie("surveycode");
	document.getElementById("SurveyCode").innerText = surveycode;
	document.getElementById("SurveyCode").style.display = "block";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}