# INFO

Survey A (with meter) is `1fe4bf6791.html`  
Survey B (without meter) is `86e74dd685.html`

This is so that when the survey is presented to a viewer, they do not know there are multiple versions